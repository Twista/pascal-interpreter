<?php
error_reporting(-1);

function autoload($name){
    if(is_readable(__DIR__.'/src/'.$name.'.php'))
        require __DIR__.'/src/'.$name.'.php';
}

spl_autoload_register('autoload');


// [E] :== [T] | [T][E_] | + [T] | - [T] | + [T][E_] | - [T][E_]
// [E_] :== + [T] | - [T] | + [T][E_] | - [T][E_]
// [T] :== [F] | [F][T_]
// [T_] :== * [F] | div [F] | mod [F] | * [F][T_] | div [F][T_] | mod [F][T_]
// [F] :== [I] | [ID] | [ID] ( ) | [ID] ( [Es] ) | ( [E] ) | break | continue
// [Es] :== [E] | [E] , [Es]


$input = "-1 + (-3)  *4   div 5*876  div      22 *(   -23).";
echo $input ."<br>";
eval("echo -1 + (int)((int)((-3) *4 / 5)*876 / 22) *( -23) .'<br>';");

$parser = new PascalParser(new PascalLexer($input));
$tree = $parser->parse();
echo $tree->accept(new Evaluator(), null) ."<br>";
echo $tree->accept(new Printer(), null) ."<br>";



// [G] :== program [ID] ; [S] . | program [ID] ; [Ds] ; [S] .

// [P] :== [H] ; forward | [H] ; [S] | [H] ; [Ds] ; [S]
// [H] :== procedure [ID] | function [ID] | procedure [ID] ( [PRs] ) | function [ID] ( [PRs] )
// [PRs] :== [IDs] : [ID] | var [IDs] : [ID] | const [IDs] : [ID]
//         | [IDs] : [ID]  , [PRs] | var [IDs] : [ID] , [PRs] | const [IDs] : [ID] , [PRs]

// [Ds] :== [V] ; | [CT] ; | [P] ; | [V] ; [Ds] | [CT] ; [Ds] | [P] ; [Ds]

// S = statement, E = expression, A = assignment, B = branch, L = loop,
// C = condition, T = term, F = factor, I = int, ID = identifier, FP = float
// P = procedure, PR = parameter, Ds = declarations, H = procedure header, G = grammar

// [S] :== [E] | [A] | [B] | [L] | begin end | begin [Ss] end | begin [Ss] ; end
// [Ss] :== [S] | [S] ; [Ss]

// [A] :== [ID] := [E]

// [B] :== if [C] then [S] | if [C] then [S] else [S] | case [E] of [B_] end
// [B_] :== [E] : [S] ; | [E] : [S] ; [B_]

// [L] :== while [C] do [S] | repeat [Ss] until [C] | for [A] to [E] do [S] | for [A] downto [E] do [S]


// [E] :== [T] | [T][E_] | + [T] | - [T] | + [T][E_] | - [T][E_]
// [E_] :== + [T] | - [T] | + [T][E_] | - [T][E_]
// [T] :== [F] | [F][T_]
// [T_] :== * [F] | div [F] | mod [F] | * [F][T_] | div [F][T_] | mod [F][T_]
// [F] :== [I] | [ID] | [ID] ( ) | [ID] ( [Es] ) | ( [E] ) | break | continue
// [Es] :== [E] | [E] , [Es]