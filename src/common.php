<?php

class TokenExpectedException extends \Exception {}
class FactorExpectedException extends \Exception {}

class UnexpectedEOFException extends \Exception {}
class InvalidCharacterException extends \Exception {}