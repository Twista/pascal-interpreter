<?php

class PascalLexer {

    // protected $tokens = array();

    protected $teminals = array(
        // keywords
        "/^program/" => "T_PROGRAM", // program
        "/^begin/" => "T_BEGIN", // begin
        "/^end/" => "T_END", // end
        "/^var/" => "T_VAR", // var

        "/^procedure/" => "T_PROCEDURE",
        "/^function/" => "T_FUNCTION",
        "/^forward/" => "T_FORWARD",
        "/^const/" => "T_CONST",
        "/^type/" => "T_TYPE",
        "/^record/" => "T_RECORD",
        "/^object/" => "T_OBJECT",
        "/^if/" => "T_IF",
        "/^then/" => "T_THEN",
        "/^else/" => "T_ELSE",
        "/^case/" => "T_CASE",
        "/^of/" => "T_OF",
        "/^while/" => "T_WHILE",
        "/^do/" => "T_DO",
        "/^repeat/" => "T_REPEAT",
        "/^until/" => "T_UNTIL",
        "/^for/" => "T_FOR",
        "/^to/" => "T_TO",
        "/^downto/" => "T_DOWNTO",
        "/^break/" => "T_BREAK",
        "/^continue/" => "T_CONTINUE",

        // types
        "/^integer/" => "T_INTEGER",
        "/^real/" => "T_REAL",
        "/^char/" => "T_CHAR",
        "/^string/" => "T_STRING",
        "/^boolean/" => "T_BOOLEAN",

        // commnets and whitespace
        "/^\s+/" => "T_WHITESPACE",
        "/^\(\*.*\*\)/" => "T_COMMENT", // must be before T_OPEN_PARENTHESIS
        "/^\{.*\}/" => "T_COMMENT",

        "/^\d*\.\d+(e[+-]?\d)?/" => "T_FLOAT", // must be before T_DOT and T_INT

        // control charactes
        "/^:=/" => "T_ASSIGNMENT",
        "/^:/" => "T_COLON", // :
        "/^\./" => "T_DOT", // .
        "/^\.\./" => "T_DOUBLE_DOT", // ..
        "/^,/" => "T_COMMA", // ,
        "/^;/" => "T_SEMICOLON", // ;
        "/^\(/" => "T_OPEN_PARENTHESIS", // (
        "/^\)/" => "T_CLOSE_PARENTHESIS", // )

        // operators
        "/^\+/" => "T_PLUS",
        "/^\-/" => "T_MINUS",
        "/^\*/" => "T_TIMES",
        "/^div/" => "T_DIVIDE",
        "/^mod/" => "T_MOD",
        "/^=/" => "T_EQUAL",
        "/^\<\>/" => "T_NOT_EQUAL",
        "/^\</" => "T_LESS_THAN",
        "/^\>/" => "T_GREATER_THAN",
        "/^\<=/" => "T_LESS_OR_EQUALS_THAN",
        "/^\=>/" => "T_EQUALS_OR_GREATER_THAN",


        "/^\'.*\'/" => "T_STRING", // 'some string'
        "/^\".*\"/" => "T_STRING", // "somestring"
        "/^\d+/" => "T_INT", // "somestring"
        "/^\w+/" => "T_IDENTIFIER",
    );

    protected $input;

    protected $current;

    public function __construct($input) {
        $this->input = $input;
        $this->next();
    }

    public function current() {
        return $this->current;
    }

    public function next() {
        // if (empty($this->input)) {
        //  throw new UnexpectedEOFException();
        // }
        do {
            $token = $this->tokenize($this->input);
            if ($token === false) {
                throw new InvalidCharacterException();
            }
            $this->input = substr($this->input, strlen($token->getContent()));
        } while (in_array($token->getName(), ["T_WHITESPACE", "T_COMMENT"]));
        echo $token->getName() . "<br>";
        return $this->current = $token;
    }

    public function valid() {
        // TODO
    }

    // public static function parse($source) {
    //  $source = explode(PHP_EOL,$source);
    //  foreach ($source as $line => $code) {
    //      $offset = 0;
    //      while($offset < strlen($code)){
    //          $result = $this->tokenize(substr($code,$offset),$line);
    //          if(!$result)
    //              throw new \Exception('Cannot parse $line '.$line.' "'.$code.'"');

    //          $this->tokens[] = array_merge($result,array('$line' => $line));
    //          $offset += strlen($result['match']);
    //      }
    //  }
    //  return $this->tokens;
    // }

    protected function tokenize($code) {
        foreach ($this->teminals as $pattern => $name) {
            if(preg_match_all($pattern, $code, $matches)) {
                return new Token($name, $matches[0][0]);
            }
        }
        return false;
    }
}