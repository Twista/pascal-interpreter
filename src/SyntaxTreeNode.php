<?php

abstract class SyntaxTreeNode {
    abstract function accept(ISyntaxTreeVisitor $visitor, $context);
}
