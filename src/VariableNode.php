<?php

class VariableNode extends SyntaxTreeNode {
    protected $name;

    function __construct($name) {
        $this->name = $name;
    }

    function getName() {
        return $this->name;
    }

    function accept(ISyntaxTreeVisitor $visitor, $context) {
        return $visitor->visitVariable($this, $context);
    }
}