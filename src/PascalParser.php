<?php

class PascalParser {

    protected $lexer;

    function __construct(PascalLexer $lexer) {
        $this->lexer = $lexer;
    }

    function parse() {
        return $this->E();
    }

    protected function E() {
        switch ($this->lexer->current()->getName()) {
            case "T_PLUS":
                $this->lexer->next();
                $node = $this->T();
                goto labelE;
            case "T_MINUS":
                $this->lexer->next();
                $node = new MinusNode(new IntegerNode(0), $this->T());
                labelE:
                switch ($this->lexer->current()->getName()) {
                    case "T_PLUS":
                    case "T_MINUS":
                        $node = $this->E_($node);
                }
                return $node;
            case "T_INT":
            case "T_OPEN_PARENTHESIS":
                $this->node = $this->T();
                switch ($lexer->current()->getName()) {
                    case "T_PLUS":
                    case "T_MINUS":
                        $node = $this->E_($node);
                }
                return $node;
        }
        $this->err();
    }

    protected function E_($left) {
        switch ($this->lexer->current()->getName()) {
            case "T_PLUS":
                $this->lexer->next();
                $node = new PlusNode($left, $this->T());
                goto labelE_;
            case "T_MINUS":
                $lexer->next();
                $node = new MinusNode($left, $this->T());
                labelE_:
                switch ($this->lexer->current()->getName()) {
                    case "T_PLUS":
                    case "T_MINUS":
                        $node = $this->E_($node);
                }
                return $node;
        }
        $this->err();
    }

    protected function T() {
        switch ($this->lexer->current()->getName()) {
            case "T_INT":
            case "T_OPEN_PARENTHESIS":
                $node = $this->F();
                switch ($this->lexer->current()->getName()) {
                    case "T_TIMES":
                    case "T_DIVIDE":
                    case "T_MODULO":
                        $node = $this->T_($node);
                }
                return $node;
        }
        $this->err();
    }

    protected function T_($left) {
        switch ($this->lexer->current()->getName()) {
            case "T_TIMES":
                $this->lexer->next();
                $node = new TimesNode($left, $this->F());
                goto labelT_;
            case "T_DIVIDE":
                $this->lexer->next();
                $node = new DivideNode($left, $this->F());
                goto labelT_;
            case "T_MODULO":
                $this->lexer->next();
                $node = new ModuloNode($left, $this->F());
                labelT_:
                switch ($this->lexer->current()->getName()) {
                    case "T_TIMES":
                    case "T_DIVIDE":
                    case "T_MODULO":
                        $node = $this->T_($node);
                }
                return $node;
        }
        $this->err();
        //throw new \Exception("syntax error");
    }

    protected function F() {
        switch ($this->lexer->current()->getName()) {
            case "T_INT":
                $node = new IntegerNode($this->lexer->current()->getContent());
                $this->lexer->next();
                return $node;
            break;
            case "T_OPEN_PARENTHESIS":
                $this->lexer->next();
                $node = $this->E();
                $this->consume("T_CLOSE_PARENTHESIS");
                return $node;
        }
        throw new FactorExpectedException();
    }

    protected function consume($token) {
        if ($this->lexer->current()->getName() === $token) {
            $this->lexer->next();
        } else {
            throw new TokenExpectedException($token);
        }
    }

    protected function err() {
        echo '<pre>'; print_r($this->lexer->current()); print_r(debug_backtrace()); echo '</pre>';
        die("syntax error");
    }
}